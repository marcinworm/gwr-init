## How-to setup GWR environment
Cheers mate, let's simplify sitecore setup.
Before you run this script, you have to be aware how it is work.

### Prerequirement
- SSH key added in bitbucket
- Powershell v5

#### Optional packages
You need to have **Git** and **Node.js** installed, if you do not have those packages,
script can handle it by you, please change boolean flag to $True, cause by default it is disabled.

```useAdditionalDependencies -continue $False```

#### Have you got text editor?
What is you favorite text editor? VSCode, atom, webstorm, brackets or maybe sublimetext3?
You can pass editor type into script also, simply change line below and do not forget about boolean flag.

```useEditor -type "vscode" -continue $False```

### Tell me more
#### Dependencies
All required dependencies will be provided by script, even environment variables.

#### I want to see the code
GWR platform contain of 3 frontend modules and brochure site, all modules will be pulled by script.
Please double check your SSH key, it is very important to have it before run script.

#### What services should be enabled?
All required services will be enabled by script.

#### Can I grab a cup of coffe?
Please take a look of our confulence page where you can find more information about platform
[Onboarding documentation](https://ormlondon.atlassian.net/wiki/spaces/FGW/pages/1034354734/Onboarding+-+FE+Dev)

### Helpful
dism /online /Get-Features - windows services states

### Source
- [Package manager for Windows](https://chocolatey.org/)
- [Package manager reference](https://github.com/chocolatey/choco/wiki/CommandsReference)
