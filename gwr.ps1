function notify {
	param ([string]$message)
	Write-Host "`n`n$message";
	$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
}

function getRepositories {
	param (
		[string]$path = "C:\Source",
		[string]$location = "git@bitbucket.org:ormlondon/",
		[array]$repos = @("gwr-booking-flow-frontend", "gwr-my-account", "gwr-mixingdeck-desktop", "gwr", "retailhub-service")
	)

	foreach ($repo in $repos) {
		if (Test-Path $path\$repo) {
			echo "repository $repo already exists"
		} else {
			echo "`ngit clone $location$repo.git $path\$repo"
			git clone "$location$repo.git" "$path\$repo"
		}
	}
}

function useServices {
	# enable Internet Information Services
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-WebServerRole
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-WebServer
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-CommonHttpFeatures
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-HttpErrors
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-HttpRedirect
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-ApplicationDevelopment

	Enable-WindowsOptionalFeature -online -FeatureName NetFx4Extended-ASPNET45
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-NetFxExtensibility45

	Enable-WindowsOptionalFeature -Online -FeatureName IIS-HealthAndDiagnostics
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-HttpLogging
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-LoggingLibraries
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-RequestMonitor
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-HttpTracing
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-Security
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-RequestFiltering
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-Performance
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-WebServerManagementTools
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-IIS6ManagementCompatibility
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-Metabase
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-ManagementConsole
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-BasicAuthentication
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-WindowsAuthentication
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-StaticContent
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-DefaultDocument
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-WebSockets
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-ApplicationInit
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-ISAPIExtensions
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-ISAPIFilter
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-HttpCompressionStatic
	Enable-WindowsOptionalFeature -Online -FeatureName IIS-ASPNET45
}

function useDependencies {
	# set admin rights
	if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -NoExit -File `"$PSCommandPath`"" -Verb RunAs; exit }

	# use windows package manager
	Set-ExecutionPolicy Bypass -Scope Process -Force; Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
	refreshenv

	choco install mssqlservermanagementstudio2014express --version 12.2.5000.20170905 --confirm --verbose
	choco install mongodb --version 4.0.4 --confirm --verbose
	choco install robo3t --version 1.2.1 --confirm --verbose
	choco install dotnet4.7.1 --confirm --verbose
	choco install kb2919355 --confirm --verbose
	choco install visualstudio2015community --version 14.0.23107.0 --confirm --verbose --execution-timeout 7200
	choco install sim --confirm --verbose
}

function useAdditionalDependencies {
	param ([bool]$continue)

	if ($continue) {
		choco install git --confirm
		choco install nodejs-lts --version 8.11.3 --confirm
		choco install nvm --confirm
	}
}

# vscode, atom, webstorm, brackets, sublimetext3
function useEditor {
	param ([string]$type, [bool]$continue)

	if ($continue) {
		choco install $type --confirm
	}
}

getRepositories
useDependencies
useAdditionalDependencies -continue $False
useEditor -type "vscode" -continue $False
useServices
refreshenv

notify -message "Please add ssh key to bitbucket, then press any key to continue ..."
notify -message "You are set up, press button to close console."

